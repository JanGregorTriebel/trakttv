<?php

require_once __DIR__.'/../vendor/autoload.php'; 

$app = new Silex\Application(); 

$app->get('/show/summary.json/{apiKey}/{title}', function($apiKey, $title) use($app) { 

    $traktUrl = sprintf('http://api.trakt.tv/show/summary.json/%s/%s', $apiKey, $title);

    $client = new GuzzleHttp\Client();
    $response = $client->get($traktUrl);

    $show = $response->json();

    if ($show['status'] == 'Continuing') {
        
        $show['next_episode'] = 0;

        $traktUrl = sprintf('http://api.trakt.tv/show/seasons.json/%s/%s', $apiKey, $title);

        $response = $client->get($traktUrl);
        $seasons  = $response->json();

        $currentSeason = array_shift($seasons);
        $seasonNumber  = $currentSeason['season'];
        
        $traktUrl = sprintf('http://api.trakt.tv/show/season.json/%s/%s/%s', $apiKey, $title, $seasonNumber);

        $response       = $client->get($traktUrl);
        $seasonEpisodes = $response->json();

        $today = new \Datetime();
        
        foreach ($seasonEpisodes as $episode) {

            $episodeDate = DateTime::createFromFormat(DateTime::ISO8601, $episode['first_aired_iso']);

            if ($today < $episodeDate) {
                $show['next_episode'] = $episode['first_aired_iso'];
                break;
            }
        }    
    }

    return $app->json($show, 200);

}); 


$app->run(); 